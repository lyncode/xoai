/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.data;

import java.util.List;

/**
 *
 * @author melo
 */
public abstract class AbstractItem extends AbstractItemIdentifier {
    /**
     * Marchable objects
     *
     * @return
     */
    public abstract List<AbstractAbout> getAbout ();
    public boolean hasAbout () {
        return (!this.getAbout().isEmpty());
    }
}
