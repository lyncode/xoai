/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class IllegalVerbException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2748244610538429452L;


	/**
     * Creates a new instance of <code>IllegalVerbException</code> without detail message.
     */
    public IllegalVerbException() {
    }


    /**
     * Constructs an instance of <code>IllegalVerbException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public IllegalVerbException(String msg) {
        super(msg);
    }
}
