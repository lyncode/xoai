/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class NoMatchesException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7051492953854730413L;


	/**
     * Creates a new instance of <code>NoMatchesException</code> without detail message.
     */
    public NoMatchesException() {
    }


    /**
     * Constructs an instance of <code>NoMatchesException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public NoMatchesException(String msg) {
        super(msg);
    }
}
