/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class FilterDoesNotExistsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 795857368865831163L;


	/**
     * Creates a new instance of <code>FilterDoesNotExistsException</code> without detail message.
     */
    public FilterDoesNotExistsException() {
    }


    /**
     * Constructs an instance of <code>FilterDoesNotExistsException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public FilterDoesNotExistsException(String msg) {
        super(msg);
    }
}
