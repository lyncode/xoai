/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.sets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lyncode.xoai.common.core.Set;
import com.lyncode.xoai.common.exceptions.ConfigurationException;
import com.lyncode.xoai.common.filter.AbstractFilter;
import com.lyncode.xoai.common.filter.FilterManager;
import com.lyncode.xoai.common.xml.xoaiconfig.BundleReference;
import com.lyncode.xoai.common.xml.xoaiconfig.Configuration.Sets;

/**
 *
 * @author melo
 */
public class StaticSetManager {
    // private static Logger log = LogManager.getLogger(StaticSetManager.class);
    private Map<String, StaticSet> _contexts;


    public StaticSetManager (Sets config, FilterManager fm) throws ConfigurationException {
        _contexts = new HashMap<String, StaticSet>();
        for (com.lyncode.xoai.common.xml.xoaiconfig.Configuration.Sets.Set s : config.getSet()) {
            List<AbstractFilter> filters = new ArrayList<AbstractFilter>();
            for (BundleReference r : s.getFilter()) {
                if (!fm.filterExists(r.getRefid()))
                    throw new ConfigurationException("Filter refered as "+r.getRefid()+" does not exists");
                filters.add(fm.getFilter(r.getRefid()));
            }
            StaticSet set = new StaticSet(filters, s.getPattern(), s.getName());
            _contexts.put(s.getId(), set);
        }
    }

    public boolean setExists (String id) {
        return this._contexts.containsKey(id);
    }

    public StaticSet getSet (String id) {
        return _contexts.get(id);
    }

    public List<Set> getSets () {
        return new ArrayList<Set>(_contexts.values());
    }
}
