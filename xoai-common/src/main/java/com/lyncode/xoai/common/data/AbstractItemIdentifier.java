/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.data;

import java.util.Date;
import java.util.List;

import com.lyncode.xoai.common.core.ReferenceSet;
import com.lyncode.xoai.common.core.XOAIContext;
import com.lyncode.xoai.common.filter.AbstractFilter;
import com.lyncode.xoai.common.sets.StaticSet;

/**
 *
 * @author melo
 */
public abstract class AbstractItemIdentifier {
    public abstract String getIdentifier ();
    /**
     * Creation, modification or deletion date
     *
     * @return
     */
    public abstract Date getDatestamp ();
    public abstract List<ReferenceSet> getSets ();
    public abstract boolean isDeleted ();


    public List<ReferenceSet> getSets (XOAIContext context) {
        List<ReferenceSet> list = this.getSets();
        List<StaticSet> listStatic = context.getStaticSets();
        for (StaticSet s : listStatic) {
            boolean filter = false;
            for (AbstractFilter f : s.getFilters()) {
                if (!f.isItemShown(this)) {
                    filter = true;
                }
            }
            if (!filter)
                list.add(s);
        }
        return list;
    }
}
