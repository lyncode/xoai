/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.filter;

/**
 *
 * @author melo
 */
public enum FilterScope {
    Context,
    MetadataFormat,
    Set,
    Query
}
