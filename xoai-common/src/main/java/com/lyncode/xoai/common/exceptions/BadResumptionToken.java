/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class BadResumptionToken extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3155813328644172294L;


	/**
     * Creates a new instance of <code>BadResumptionToken</code> without detail message.
     */
    public BadResumptionToken() {
    }


    /**
     * Constructs an instance of <code>BadResumptionToken</code> with the specified detail message.
     * @param msg the detail message.
     */
    public BadResumptionToken(String msg) {
        super(msg);
    }
}
