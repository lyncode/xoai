/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.lyncode.xoai.common.xml.xoaiconfig.Parameter;

/**
 *
 * @author melo
 */
public class Parameters {
    private Map<String, List<String>> _params;

    public Parameters (List<Parameter> param) {
        _params = new HashMap<String, List<String>>();
        for (Parameter p : param)
            _params.put(p.getKey(), p.getValue());
    }

    public List<String> getParameter (String key) {
        return _params.get(key);
    }

    public boolean hasParameter (String key) {
        return _params.containsKey(key);
    }

    public String getFirstParameter (String key) {
        if (this.hasParameter(key) && this.getParameter(key).size()>0)
            return this.getParameter(key).get(0);
        else return null;
    }
    
    public Set<String> getKeys () {
        return this._params.keySet();
    }
}
