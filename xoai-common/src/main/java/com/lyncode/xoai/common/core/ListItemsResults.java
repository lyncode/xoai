/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

import com.lyncode.xoai.common.data.AbstractItem;

import java.util.List;

/**
 *
 * @author melo
 */
public class ListItemsResults {
    private boolean hasMore;
    private List<AbstractItem> results;

    public ListItemsResults (boolean hasMoreResults, List<AbstractItem> results) {
        this.hasMore = hasMoreResults;
        this.results = results;
    }

    public boolean hasMore() {
        return hasMore;
    }

    public List<AbstractItem> getResults() {
        return results;
    }

}
