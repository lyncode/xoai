/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class CannotDisseminateRecordException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7121029109566433474L;


	/**
     * Creates a new instance of <code>CannotDisseminateRecordException</code> without detail message.
     */
    public CannotDisseminateRecordException() {
    }


    /**
     * Constructs an instance of <code>CannotDisseminateRecordException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public CannotDisseminateRecordException(String msg) {
        super(msg);
    }
}
