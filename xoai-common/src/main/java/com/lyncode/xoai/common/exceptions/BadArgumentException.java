/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class BadArgumentException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6436751364163509217L;


	/**
     * Creates a new instance of <code>BadArgumentException</code> without detail message.
     */
    public BadArgumentException() {
    }


    /**
     * Constructs an instance of <code>BadArgumentException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public BadArgumentException(String msg) {
        super(msg);
    }
}
