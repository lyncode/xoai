/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.xml;

import com.sun.xml.internal.bind.marshaller.NamespacePrefixMapper;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author melo
 */
@SuppressWarnings("restriction")
public class PrefixMapper extends NamespacePrefixMapper {
    private Map<String, String> _prefix;

    public PrefixMapper () {
        _prefix = new HashMap<String, String>();
    }

    public void addPrefix (String namespace, String prefix) {
        _prefix.put(namespace, prefix);
    }

    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        if (_prefix.containsKey(namespaceUri)) return _prefix.get(namespaceUri);
        return suggestion;
    }

    @Override
    public String[] getPreDeclaredNamespaceUris() {
        return _prefix.keySet().toArray(new String[_prefix.size()]);
    }
}
