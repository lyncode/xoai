/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

/**
 *
 * @author melo
 */
public class ReferenceSet {
    private String _setSpec;

    public ReferenceSet (String setSpec) {
        _setSpec = setSpec;
    }

    public String getSetSpec () {
        return _setSpec;
    }


    public boolean equals (ReferenceSet set) {
        return (_setSpec.equals(set.getSetSpec()));
    }
}
