/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

import java.util.List;

/**
 *
 * @author melo
 */
public class ListSetsResult {
    private boolean hasMore;
    private List<Set> results;

    public ListSetsResult (boolean hasMoreResults, List<Set> results) {
        this.hasMore = hasMoreResults;
        this.results = results;
    }

    public boolean hasMore() {
        return hasMore;
    }

    public List<Set> getResults() {
        return results;
    }

    
}
