/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

import com.lyncode.xoai.common.data.AbstractItemIdentifier;

import java.util.List;

/**
 *
 * @author melo
 */
public class ListItemIdentifiersResult {
    private boolean hasMore;
    private List<AbstractItemIdentifier> results;

    public ListItemIdentifiersResult (boolean hasMoreResults, List<AbstractItemIdentifier> results) {
        this.hasMore = hasMoreResults;
        this.results = results;
    }

    public boolean hasMore() {
        return hasMore;
    }

    public List<AbstractItemIdentifier> getResults() {
        return results;
    }
}
