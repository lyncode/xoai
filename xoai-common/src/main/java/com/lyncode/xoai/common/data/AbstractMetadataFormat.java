/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.data;

import com.lyncode.xoai.common.core.ConfigurableBundle;
import com.lyncode.xoai.common.core.XOAIContext;
import com.lyncode.xoai.common.filter.AbstractFilter;
import com.lyncode.xoai.common.xml.xoaiconfig.Parameter;

import java.util.List;

/**
 *
 * @author melo
 */
public abstract class AbstractMetadataFormat extends ConfigurableBundle {
    private String prefix;
    private String namespace;
    private String schemaLocation;
    private List<AbstractFilter> _list;

    public AbstractMetadataFormat (String namespace, String schemaLocation) {
        this.namespace = namespace;
        this.schemaLocation = schemaLocation;
    }

    public void loadFilters (List<AbstractFilter> filters) {
        this._list = filters;
    }

    public List<AbstractFilter> getFilters() {
        return _list;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSchemaLocation() {
        return schemaLocation;
    }

    

    public String getXML (XOAIContext context, AbstractItem item) {
        return this.getXML(context.getTransformer().transform(item));
    }

    protected abstract String getXML (AbstractItem item);

    public boolean isApplyable (AbstractItemIdentifier item) {
        if (item.isDeleted()) return true;
        for (AbstractFilter filter : this.getFilters())
            if (!filter.isItemShown(item))
                return false;
        return true;
    }

    public void load(String prefix, List<Parameter> parameter) {
        super.load(parameter);
        this.prefix = prefix;
    }
}
