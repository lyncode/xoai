/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

/**
 *
 * @author melo
 */
public class Set extends ReferenceSet {
    private String setName;
    private String description;

    public Set(String setSpec, String setName) {
        super(setSpec);
        this.setName = setName;
        this.description =null;
    }

    /**
     *
     * @param setSpec
     * @param setName
     * @param description Marshable object
     */
    public Set(String setSpec, String setName, String xmldescription) {
        this(setSpec, setName);
        this.description = xmldescription;
    }

    public String getSetName() {
        return setName;
    }

    public String getDescription() {
        return description;
    }

    public boolean hasDescription () {
        return (this.description != null);
    }
}
