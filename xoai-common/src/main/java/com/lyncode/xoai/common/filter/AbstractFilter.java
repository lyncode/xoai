/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.filter;

import com.lyncode.xoai.common.core.ConfigurableBundle;
import com.lyncode.xoai.common.data.AbstractItemIdentifier;

/**
 *
 * @author melo
 */
public abstract class AbstractFilter extends ConfigurableBundle {
    public abstract boolean isItemShown (AbstractItemIdentifier item);
}
