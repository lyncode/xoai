/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class DoesNotSupportSetsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7008970964208110045L;


	/**
     * Creates a new instance of <code>DoesNotSupportSetsException</code> without detail message.
     */
    public DoesNotSupportSetsException() {
    }


    /**
     * Constructs an instance of <code>DoesNotSupportSetsException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public DoesNotSupportSetsException(String msg) {
        super(msg);
    }
}
