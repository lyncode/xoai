/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lyncode.xoai.common.exceptions.ConfigurationException;
import com.lyncode.xoai.common.xml.xoaiconfig.Configuration.Filters;

/**
 *
 * @author melo
 */
public class FilterManager {
    // private static Logger log = LogManager.getLogger(FilterManager.class);
    private Map<String, AbstractFilter> _contexts;


    public FilterManager (Filters filters) throws ConfigurationException {
        _contexts = new HashMap<String, AbstractFilter>();
        for (com.lyncode.xoai.common.xml.xoaiconfig.Configuration.Filters.Filter f : filters.getFilter()) {
            try {
                Class<?> c = Class.forName(f.getClazz());
                Object obj = c.newInstance();
                if (obj instanceof AbstractFilter) {
                    ((AbstractFilter) obj).load(f.getParameter());
                    _contexts.put(f.getId(), (AbstractFilter) obj);
                }
            } catch (InstantiationException ex) {
                throw new ConfigurationException(ex.getMessage(), ex);
            } catch (IllegalAccessException ex) {
                throw new ConfigurationException(ex.getMessage(), ex);
            } catch (ClassNotFoundException ex) {
                throw new ConfigurationException(ex.getMessage(), ex);
            }
        }
    }

    public boolean filterExists (String id) {
        return this._contexts.containsKey(id);
    }

    public AbstractFilter getFilter (String id) {
        return _contexts.get(id);
    }

    public List<AbstractFilter> getFilters() {
        return new ArrayList<AbstractFilter>(_contexts.values());
    }
}
