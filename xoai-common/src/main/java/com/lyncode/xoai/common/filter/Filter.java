/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.filter;

/**
 *
 * @author melo
 */
public final class Filter {
    private AbstractFilter _filter;
    private FilterScope _scope;

    public Filter (AbstractFilter filter, FilterScope scope) {
        _filter = filter;
        _scope = scope;
    }

    public AbstractFilter getFilter() {
        return _filter;
    }

    public FilterScope getScope() {
        return _scope;
    }
}
