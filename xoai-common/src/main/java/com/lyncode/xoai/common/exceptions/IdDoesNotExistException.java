/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class IdDoesNotExistException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -657866486396669641L;


	/**
     * Creates a new instance of <code>IdDoesNotExistException</code> without detail message.
     */
    public IdDoesNotExistException() {
    }


    /**
     * Constructs an instance of <code>IdDoesNotExistException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public IdDoesNotExistException(String msg) {
        super(msg);
    }
}
