/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class NoMetadataFormatsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7091872607176190034L;


	/**
     * Creates a new instance of <code>NoMetadataFormatsException</code> without detail message.
     */
    public NoMetadataFormatsException() {
    }


    /**
     * Constructs an instance of <code>NoMetadataFormatsException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public NoMetadataFormatsException(String msg) {
        super(msg);
    }
}
