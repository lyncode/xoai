/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

import com.lyncode.xoai.common.configuration.ConfigurationManager;
import com.lyncode.xoai.common.exceptions.ConfigurationException;
import com.lyncode.xoai.common.filter.FilterManager;
import com.lyncode.xoai.common.format.MetadataFormatManager;
import com.lyncode.xoai.common.sets.StaticSetManager;
import com.lyncode.xoai.common.transform.TransformManager;
import com.lyncode.xoai.common.xml.xoaiconfig.Configuration;

/**
 *
 * @author melo
 */
public class XOAIManager {
    private static XOAIManager _manager = null;
    public static XOAIManager getManager () {
        return _manager;
    }
    
    public static void initialize (String filename) throws ConfigurationException {
        Configuration config = ConfigurationManager.readConfiguration(filename);
        _manager = new XOAIManager(config);
    }


    private FilterManager _filter;
    private ContextManager _context;
    private TransformManager _transformer;
    private MetadataFormatManager _format;
    private StaticSetManager _set;
    private int _listSetsSize;
    private int _listRecordsSize;
    private int _listIdentifiersSize;
    private boolean _identation;

    private XOAIManager (Configuration config) throws ConfigurationException {
        _filter = new FilterManager(config.getFilters());
        _transformer = new TransformManager(config.getTransformers());
        _format = new MetadataFormatManager(config.getFormats(), _filter);
        _set = new StaticSetManager(config.getSets(), _filter);
        _listSetsSize = config.getMaxListSetsSize();
        _listIdentifiersSize = config.getMaxListRecordsSize();
        _listRecordsSize = config.getMaxListRecordsSize();
        _identation = config.isIdentation();
        _context = new ContextManager(config.getContexts(), _filter, _transformer, _format, _set);
    }

    public ContextManager getContextManager() {
        return _context;
    }

    public FilterManager getFilterManager() {
        return _filter;
    }

    public MetadataFormatManager getFormatManager() {
        return _format;
    }

    public StaticSetManager getSetManager() {
        return _set;
    }

    public TransformManager getTransformerManager() {
        return _transformer;
    }

    public int getMaxListIdentifiersSize() {
        return _listIdentifiersSize;
    }

    public int getMaxListRecordsSize() {
        return _listRecordsSize;
    }

    public int getMaxListSetsSize() {
        return _listSetsSize;
    }

    public boolean isIdentated() {
        return _identation;
    }


}
