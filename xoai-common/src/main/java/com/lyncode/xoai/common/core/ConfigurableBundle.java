/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

import com.lyncode.xoai.common.configuration.Parameters;
import com.lyncode.xoai.common.xml.xoaiconfig.Parameter;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author melo
 */
public abstract class ConfigurableBundle {
    private static Logger log = LogManager.getLogger(ConfigurableBundle.class);
    private Parameters _parameters;

    public void load (List<Parameter> parameters) {
        _parameters = new Parameters(parameters);
    }
    
    public List<String> getParameters (String key) {
        if (!_parameters.hasParameter(key))
            return new ArrayList<String>();
        return _parameters.getParameter(key);
    }

    public String getParameter (String key) {
        String p =  _parameters.getFirstParameter(key);
        log.debug("Parameter: "+key+" = "+p);
        return p;
    }

    public String getParameter (String key, String none) {
        if (!hasParameter(key)) return none;
        String p =  _parameters.getFirstParameter(key);
        log.debug("Parameter: "+key+" = "+p);
        return p;
    }

    public boolean hasParameter (String key) {
        return _parameters.hasParameter(key);
    }

    public List<String> getKeys () {
        return new ArrayList<String>(_parameters.getKeys());
    }
}
