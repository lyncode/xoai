/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class MetadataFormatDoesNotExistsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7169710535202336338L;


	/**
     * Creates a new instance of <code>MetadataFormatDoesNotExistsException</code> without detail message.
     */
    public MetadataFormatDoesNotExistsException() {
    }


    /**
     * Constructs an instance of <code>MetadataFormatDoesNotExistsException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public MetadataFormatDoesNotExistsException(String msg) {
        super(msg);
    }
}
