/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author melo
 */
public class Metadata {
    private Map<String, List<String>> _metadata;

    public Metadata() {
        _metadata = new HashMap<String, List<String>>();
    }

    public void add (String path, String value) {
        if (_metadata.get(path) == null)
            _metadata.put(path, new ArrayList<String>());
        _metadata.get(path).add(value);
    }

    public List<String> get (String path) {
        return _metadata.get(path);
    }

    public boolean hasMetadataEntry (String path) {
        return _metadata.containsKey(path);
    }

    public String getFirst (String path) {
        if (_metadata.get(path) != null && _metadata.get(path).size() > 0)
            return _metadata.get(path).get(0);
        else
            return null;
    }

    public List<String> getPaths () {
        return new ArrayList<String>(_metadata.keySet());
    }
}
