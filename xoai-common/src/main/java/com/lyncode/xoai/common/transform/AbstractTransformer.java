/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.transform;

import com.lyncode.xoai.common.core.ConfigurableBundle;
import com.lyncode.xoai.common.data.AbstractItem;

/**
 *
 * @author melo
 */
public abstract class AbstractTransformer extends ConfigurableBundle {
    public abstract AbstractItem transform (AbstractItem item);
}
