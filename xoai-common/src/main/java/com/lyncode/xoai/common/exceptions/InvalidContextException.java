/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class InvalidContextException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2541049223278976241L;


	/**
     * Creates a new instance of <code>InvalidContextException</code> without detail message.
     */
    public InvalidContextException() {
    }


    /**
     * Constructs an instance of <code>InvalidContextException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public InvalidContextException(String msg) {
        super(msg);
    }
}
