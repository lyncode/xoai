/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class TransformerDoesNotExistsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8417020535730995337L;


	/**
     * Creates a new instance of <code>TransformerDoesNotExistsException</code> without detail message.
     */
    public TransformerDoesNotExistsException() {
    }


    /**
     * Constructs an instance of <code>TransformerDoesNotExistsException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public TransformerDoesNotExistsException(String msg) {
        super(msg);
    }
}
