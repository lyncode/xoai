/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.exceptions;

/**
 *
 * @author melo
 */
public class StaticSetDoesNotExistsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2596586736462175699L;


	/**
     * Creates a new instance of <code>StaticSetDoesNotExistsException</code> without detail message.
     */
    public StaticSetDoesNotExistsException() {
    }


    /**
     * Constructs an instance of <code>StaticSetDoesNotExistsException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public StaticSetDoesNotExistsException(String msg) {
        super(msg);
    }
}
