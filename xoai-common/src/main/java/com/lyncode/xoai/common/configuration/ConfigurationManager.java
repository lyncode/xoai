/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.configuration;

import com.lyncode.xoai.common.exceptions.ConfigurationException;
import com.lyncode.xoai.common.xml.xoaiconfig.Configuration;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author melo
 */
public class ConfigurationManager {
    public static Configuration readConfiguration (String filename) throws ConfigurationException {
        try {
            JAXBContext context = JAXBContext.newInstance("com.lyncode.xoai.common.xml.xoaiconfig");
            Unmarshaller marshaller = context.createUnmarshaller();
            FileInputStream reader = new FileInputStream(filename);
            Object obj = marshaller.unmarshal(reader);
            reader.close();
            if (obj instanceof Configuration) {
                return (Configuration) obj;
            } else throw new ConfigurationException("Invalid configuration bundle");
        } catch (IOException ex) {
            throw new ConfigurationException(ex.getMessage(), ex);
        } catch (JAXBException ex) {
            throw new ConfigurationException(ex.getMessage(), ex);
        }
    }

    public static void writeConfiguration (Configuration config, String filename) throws ConfigurationException {
        try {
            JAXBContext context = JAXBContext.newInstance("com.XOAI.configuration.xml");
            Marshaller marshaller = context.createMarshaller();
            FileOutputStream writer = new FileOutputStream(filename);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(config, writer);
            writer.close();
        } catch (IOException ex) {
            throw new ConfigurationException(ex.getMessage(), ex);
        } catch (JAXBException ex) {
            throw new ConfigurationException(ex.getMessage(), ex);
        }
    }
}
