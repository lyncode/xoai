/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.transform;

import java.util.HashMap;
import java.util.Map;

import com.lyncode.xoai.common.exceptions.ConfigurationException;
import com.lyncode.xoai.common.xml.xoaiconfig.Configuration.Transformers;
import com.lyncode.xoai.common.xml.xoaiconfig.Configuration.Transformers.Transformer;

/**
 *
 * @author melo
 */
public class TransformManager {
    //private static Logger log = LogManager.getLogger(TransformManager.class);
    private Map<String, AbstractTransformer> _contexts;


    public TransformManager (Transformers config) throws ConfigurationException {
        _contexts = new HashMap<String, AbstractTransformer>();
        try {
            for (Transformer t : config.getTransformer()) {
                Class<?> c = Class.forName(t.getClazz());
                Object obj = c.newInstance();
                if (obj instanceof AbstractTransformer) {
                    ((AbstractTransformer) obj).load(t.getParameter());
                    _contexts.put(t.getId(), (AbstractTransformer) obj);
                }
            }
        } catch (ClassNotFoundException ex) {
            throw new ConfigurationException(ex.getMessage(), ex);
        } catch (InstantiationException ex) {
            throw new ConfigurationException(ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            throw new ConfigurationException(ex.getMessage(), ex);
        }
    }

    public boolean transformerExists (String id) {
        return this._contexts.containsKey(id);
    }

    public AbstractTransformer getTransformer (String id) {
        return _contexts.get(id);
    }

}
