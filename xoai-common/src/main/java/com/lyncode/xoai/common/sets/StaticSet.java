/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lyncode.xoai.common.sets;

import com.lyncode.xoai.common.core.Set;
import com.lyncode.xoai.common.filter.AbstractFilter;

import java.util.List;


/**
 *
 * @author melo
 */
public class StaticSet extends Set {
    private List<AbstractFilter> _filters;

    public StaticSet (List<AbstractFilter> filters, String spec, String name) {
        super(spec, name);
        _filters = filters;
    }

    public List<AbstractFilter> getFilters() {
        return _filters;
    }
}
