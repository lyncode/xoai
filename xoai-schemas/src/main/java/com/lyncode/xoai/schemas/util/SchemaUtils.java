package com.lyncode.xoai.schemas.util;

import java.io.ByteArrayOutputStream;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.lyncode.xoai.common.xml.PrefixMapper;
import com.lyncode.xoai.schemas.etdms.Thesis;
import com.lyncode.xoai.schemas.oaidc.OaiDcType;

public class SchemaUtils {
	private static Logger log = LogManager.getLogger(SchemaUtils.class);


    public static String export (OaiDcType xml) {
        try {
        	com.lyncode.xoai.schemas.oaidc.ObjectFactory fac = new com.lyncode.xoai.schemas.oaidc.ObjectFactory();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            JAXBContext context = JAXBContext.newInstance("com.lyncode.xoai.schemas.oaidc");
            Marshaller marshaller = context.createMarshaller();
            PrefixMapper mapper = new PrefixMapper();
            mapper.addPrefix("http://www.openarchives.org/OAI/2.0/oai_dc/", "oai_dc");
            mapper.addPrefix("http://purl.org/dc/elements/1.1/", "dc");
            marshaller.setProperty("com.sun.xml.internal.bind.namespacePrefixMapper", mapper);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(fac.createDc(xml), output);
            return output.toString().replaceAll(Pattern.quote("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"), "");
        } catch (JAXBException ex) {
            log.error(ex.getMessage(), ex);
            return "";
        }
    }
    

    public static String export (Thesis xml) {
        try {
        	ByteArrayOutputStream output = new ByteArrayOutputStream();
            JAXBContext context = JAXBContext.newInstance("com.lyncode.xoai.schemas.etdms");
            Marshaller marshaller = context.createMarshaller();
            PrefixMapper mapper = new PrefixMapper();
            mapper.addPrefix("http://www.ndltd.org/standards/metadata/etdms/1.0/", "etdms");
            mapper.addPrefix("http://purl.org/dc/elements/1.1/", "dc");
            marshaller.setProperty("com.sun.xml.internal.bind.namespacePrefixMapper", mapper);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(xml, output);
            return output.toString().replaceAll(Pattern.quote("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"), "");
        } catch (JAXBException ex) {
            log.error(ex.getMessage(), ex);
            return "";
        }
    }
}
